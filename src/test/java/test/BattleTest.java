package test;

import no.ntnu.folk.jonmro.army.Army;
import no.ntnu.folk.jonmro.battle.Battle;
import no.ntnu.folk.jonmro.units.*;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class BattleTest {

    @Test
    void simulate() {
        ArrayList<Unit> testArmyOne = new ArrayList<>();
        ArrayList<Unit> testArmyTwo = new ArrayList<>();

        for (int i = 0; i < 500; i++) {
            testArmyOne.add(new InfantryUnit("Soldier", 100));
            testArmyTwo.add(new InfantryUnit("Soldier", 100));
        }
        for (int i = 0; i < 100; i++) {
            testArmyOne.add(new CavalryUnit("Horse", 100));
            testArmyTwo.add(new CavalryUnit("Horse", 100));
        }
        for (int i = 0; i < 200; i++) {
            testArmyOne.add(new RangedUnit("Archer", 100));
            testArmyTwo.add(new RangedUnit("archer", 100));
        }
        for (int i = 0; i < 1; i++) {
            testArmyOne.add(new CommanderUnit("Leader", 180));
            testArmyTwo.add(new CommanderUnit("Leader", 180));
        }

        Army armyOne = new Army("testArmyOne", testArmyOne);
        Army armyTwo = new Army("testArmyTwo", testArmyTwo);

        Battle battle = new Battle(armyOne, armyTwo);

        Army result = battle.simulate();

        assertTrue(result != null);
    }

    @Test
    void armyTwoWins() {
        Army armyOne = new Army("armyOne");
        Army armyTwo = new Army("armyTwo");
        armyOne.add(new InfantryUnit("test", 1));
        armyTwo.add(new InfantryUnit("test", 1));
        Battle battle = new Battle(armyOne, armyTwo);
        armyOne.remove(armyOne.getRandom());
        assertEquals(armyTwo, battle.simulate());
    }
    @Test
    void noUnits(){
        Army armyOne = new Army("armyOne");
        Army armyTwo = new Army("armyTwo");
        armyOne.add(new InfantryUnit("test", 1));
        armyTwo.add(new InfantryUnit("test", 1));
        armyOne.remove(armyOne.getRandom());
        assertThrows(
                IllegalArgumentException.class, () ->{
                Battle battle = new Battle(armyOne, armyTwo);
        });


    }
    
}