package test;

import no.ntnu.folk.jonmro.army.Army;
import org.junit.jupiter.api.Test;
import no.ntnu.folk.jonmro.units.InfantryUnit;
import no.ntnu.folk.jonmro.units.Unit;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class ArmyTest {

    @Test
    void getName() {
        Army army = new Army("Test");
        assertEquals("Test", army.getName());
    }

    @Test
    void add() {
        Army army = new Army("Test");
        army.add(new InfantryUnit("test", 100));
        assertTrue(army.hasUnits());
    }

    @Test
    void addAll() {
        int numUnits = 500;
        Army army = new Army("Test");
        ArrayList<Unit> units = new ArrayList<Unit>();
        for (int i = 0; i < numUnits; i++) {
            units.add(new InfantryUnit("test", 100));
        }
        army.addAll(units);
        assertEquals(numUnits, army.getAllUnits().size());
    }

    @Test
    void remove() {
        Army army = new Army("Test");
        Unit unit = new InfantryUnit("test", 100);
        army.add(unit);
        army.remove(unit);
        assertFalse(army.hasUnits());
    }

    @Test
    void hasUnits() {
        Army army = new Army("Test");
        assertFalse(army.hasUnits());
        Unit unit = new InfantryUnit("test", 100);
        army.add(unit);
        assertTrue(army.hasUnits());
    }

    @Test
    void getAllUnits() {
        int numUnits = 500;
        Army army = new Army("Test");
        ArrayList<Unit> units = new ArrayList<Unit>();
        for (int i = 0; i < numUnits; i++) {
            units.add(new InfantryUnit("test", 100));
        }
        army.addAll(units);
        assertEquals(units, army.getAllUnits());
    }
}