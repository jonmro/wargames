package test;

import org.junit.jupiter.api.Test;
import no.ntnu.folk.jonmro.units.InfantryUnit;
import no.ntnu.folk.jonmro.units.Unit;

import static org.junit.jupiter.api.Assertions.*;

class InfantryUnitTest {

    @Test
    void attack() {
        Unit unit1 = new InfantryUnit("test1",100);
        Unit unit2 = new InfantryUnit("test2",100);
        unit1.attack(unit2);
        assertNotEquals(100, unit2.getHealth());
    }

    @Test
    void getName() {
        Unit unit1 = new InfantryUnit("test1",100);
        assertEquals("test1", unit1.getName());
    }

    @Test
    void getHealth() {
        Unit unit1 = new InfantryUnit("test1",100);
        assertEquals(100, unit1.getHealth());
    }


    @Test
    void setHealth() {
        Unit unit1 = new InfantryUnit("test1",100);
        unit1.setHealth(120);
        assertEquals(120, unit1.getHealth());
    }

    @Test
    void getAttackBonus() {
        Unit unit1 = new InfantryUnit("test1",100);
        assertEquals(2, unit1.getAttackBonus());
    }

    @Test
    void getResistBonus() {
        Unit unit1 = new InfantryUnit("test1",100);
        assertEquals(1, unit1.getResistBonus());
    }
}