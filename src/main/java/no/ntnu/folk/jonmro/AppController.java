package no.ntnu.folk.jonmro;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import no.ntnu.folk.jonmro.army.Army;
import no.ntnu.folk.jonmro.battle.Battle;
import no.ntnu.folk.jonmro.units.Unit;
import no.ntnu.folk.jonmro.units.UnitFactory;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Controller for JavaFX GUI
 * Responding to user input in GUI
 */
public class AppController {


    /// Game Variables
    private static String terrain;
    private static Army armyOne;
    private static Army armyTwo;
    private Battle battle;
    // shows which army has been created manually
    private int createClick;
    ///

    // export.fxml
    @FXML
    private TextField ExportArmyOnePath;
    @FXML
    private TextField ExportArmyTwoPath;
    @FXML
    private Button ExportExport;
    @FXML
    private TextArea ExportTextArea;
    // import.fxml
    @FXML
    private TextField ImportArmyOnePath;
    @FXML
    private TextField ImportArmyTwoPath;
    @FXML
    private Button ImportImport;
    @FXML
    private TextArea ImportTextArea;
    // input-army.fxml
    @FXML
    private TextField InputArmyName;
    @FXML
    private TextField InputAttributes;
    @FXML
    private Button InputCreateArmy;
    @FXML
    private TextArea InputMessage;
    @FXML
    private Button InputBack;
    @FXML
    private Button InputFinish;
    // menu.fxml
    @FXML
    private Button menuSummary;
    @FXML
    private Button menuExport;
    @FXML
    private Button menuImport;
    @FXML
    private Button menuStandardArmy;
    @FXML
    private Button menuInputArmy;
    @FXML
    private Button menuBattle;
    @FXML
    private Text menuText;

    // battle.fxml
    @FXML
    private Button BattleHILL;
    @FXML
    private Button BattleFOREST;
    @FXML
    private Button BattlePLAINS;
    @FXML
    private Button BattleBattle;
    @FXML
    private Button BattleBack;
    @FXML
    private Text BattleText;
    // results.fxml
    @FXML
    private Text ResultsTextArea;
    @FXML
    private Button ResultsReset;
    @FXML
    private Text SummaryText;


    // menu.fxml
    @FXML
    public void handleButtonMenuSummary(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("summary.fxml"));
        Scene scene = new Scene(root);

        Stage stage = (Stage) ((Node) event.getTarget()).getScene().getWindow();
        stage.setScene(scene);
    }

    @FXML
    public void handleButtonMenuExport(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("export.fxml"));
        Scene scene = new Scene(root);

        Stage stage = (Stage) ((Node) event.getTarget()).getScene().getWindow();
        stage.setScene(scene);
    }

    @FXML
    public void handleButtonMenuImport(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("import.fxml"));
        Scene scene = new Scene(root);

        Stage stage = (Stage) ((Node) event.getTarget()).getScene().getWindow();
        stage.setScene(scene);
    }

    @FXML
    public void handleButtonMenuCreateStandard() {
        ArrayList<Unit> units = new ArrayList<>();
        units.addAll(UnitFactory.createUnits("InfantryUnit", "Conscript", 100, 1000));
        units.addAll(UnitFactory.createUnits("RangedUnit", "Bowman", 70, 500));
        units.addAll(UnitFactory.createUnits("CavalryUnit", "Horseman", 150, 200));
        units.add(UnitFactory.createUnit("CommanderUnit", "King", 300));
        this.armyOne = new Army("Humans", units);

        ArrayList<Unit> units2 = new ArrayList<>();
        units2.addAll(UnitFactory.createUnits("InfantryUnit", "Grunt", 100, 1000));
        units2.addAll(UnitFactory.createUnits("RangedUnit", "Hurler", 70, 500));
        units2.addAll(UnitFactory.createUnits("CavalryUnit", "Wild-man", 150, 200));
        units2.add(UnitFactory.createUnit("CommanderUnit", "Chief", 300));
        this.armyTwo = new Army("Orcs", units2);
        menuText.setText("Success!");

    }

    @FXML
    public void handleButtonMenuInputArmy(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("input-army.fxml"));
        Scene scene = new Scene(root);

        Stage stage = (Stage) ((Node) event.getTarget()).getScene().getWindow();
        stage.setScene(scene);
    }

    @FXML
    public void handleButtonMenuBattle(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("battle.fxml"));
        Scene scene = new Scene(root);

        Stage stage = (Stage) ((Node) event.getTarget()).getScene().getWindow();
        stage.setScene(scene);
    }

    // export.fxml
    @FXML
    public void handleButtonExportExport() {
        this.armyOne.saveArmy(this.ExportArmyOnePath.getText());
        this.armyOne.saveArmy(this.ExportArmyTwoPath.getText());
        this.ExportTextArea.setText("Export Successful");
    }

    // results.fxml
    @FXML
    public void ResultsResults(MouseEvent mouseEvent) {
        this.battle = new Battle(this.armyOne, this.armyTwo, this.terrain);
        Army victor = this.battle.simulate();
        ResultsTextArea.setText(victor.getName() + " are the victorious!\n" + victor.getName() + " has " + victor.getAllUnits().size() + " units left.");
    }
    @FXML
    public void handleButtonResultsReset(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("menu.fxml"));
        Scene scene = new Scene(root);

        Stage stage = (Stage) ((Node) event.getTarget()).getScene().getWindow();
        stage.setScene(scene);
    }

    // import.fxml
    @FXML
    public void handleButtonImportImport() {
        this.armyOne = new Army("");
        this.armyOne.loadArmy(ImportArmyOnePath.getText());
        this.armyTwo = new Army("");
        this.armyTwo.loadArmy(ImportArmyTwoPath.getText());

        this.ImportTextArea.setText(this.armyOne.toString() + "\n" + this.armyTwo.toString() + "\n" + "Armies were loaded successfully");

    }
    // input-army.fxml
    @FXML
    public void handleButtonInputCreateArmy() {
        String delimiter = ",";
        if (createClick == 0) {
            this.armyOne =new Army(this.InputArmyName.getText());
            String[] lineSplit = this.InputAttributes.getCharacters().toString().split(delimiter);
            this.armyOne.addAll(UnitFactory.createUnits(lineSplit[0], lineSplit[1], Integer.parseInt(lineSplit[2]), Integer.parseInt(lineSplit[3])));
            createClick = 1;
        } else if (createClick == 1) {
            this.armyTwo =new Army(this.InputArmyName.getText());
            String[] lineSplit = this.InputAttributes.getCharacters().toString().split(delimiter);
            this.armyOne.addAll(UnitFactory.createUnits(lineSplit[0], lineSplit[1], Integer.parseInt(lineSplit[2]), Integer.parseInt(lineSplit[3])));
            createClick = 0;
        }

    }

    @FXML
    public void handleButtonInputFinish(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("menu.fxml"));
        Scene scene = new Scene(root);

        Stage stage = (Stage) ((Node) event.getTarget()).getScene().getWindow();
        stage.setScene(scene);
    }
    // summary.fxml

    public void handleButtonSummaryFullSummary(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("full-summary.fxml"));
        Scene scene = new Scene(root);

        Stage stage = (Stage) ((Node) event.getTarget()).getScene().getWindow();
        stage.setScene(scene);
    }

    //full-summary.fxml
    @FXML
    public void handleButtonFullSummaryBack(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("summary.fxml"));
        Scene scene = new Scene(root);

        Stage stage = (Stage) ((Node) event.getTarget()).getScene().getWindow();
        stage.setScene(scene);
    }


    // general buttons handling
    @FXML
    public void handleButtonBack(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("menu.fxml"));
        Scene scene = new Scene(root);

        Stage stage = (Stage) ((Node) event.getTarget()).getScene().getWindow();
        stage.setScene(scene);
    }

    // battle.fxml
    @FXML
    public void handleButtonBattleBattle(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("results.fxml"));
        Scene scene = new Scene(root);

        Stage stage = (Stage) ((Node) event.getTarget()).getScene().getWindow();
        stage.setScene(scene);
    }

    @FXML
    public void handleButtonBattlePLAINS(ActionEvent event) {
        this.terrain = "PLAINS";
        BattleText.setText("Terrain is now PLAINS.");
    }

    @FXML
    public void handleButtonBattleFOREST(ActionEvent event) {
        this.terrain = "FOREST";
        BattleText.setText("Terrain is now FOREST.");
    }

    @FXML
    public void handleButtonBattleHILL(ActionEvent event) {
        this.terrain = "HILL";
        BattleText.setText("Terrain is now HILL.");
    }

    public void handleButtonSummary(MouseEvent mouseEvent) {
        this.SummaryText.setText(this.armyOne.toString() + "\n" + this.armyTwo.toString());
    }
}

