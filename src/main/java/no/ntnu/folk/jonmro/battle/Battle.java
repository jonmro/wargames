package no.ntnu.folk.jonmro.battle;

import no.ntnu.folk.jonmro.army.Army;
import no.ntnu.folk.jonmro.units.Unit;

import java.util.Random;

/**
 *
 */
public class Battle {
    private Army armyOne;
    private Army armyTwo;
    private String terrain;


    /**
     * @param armyOne
     * @param armyTwo
     * @throws IllegalArgumentException
     */
    public Battle(Army armyOne, Army armyTwo, String terrain) throws IllegalArgumentException{
        if (!armyOne.hasUnits() || !armyTwo.hasUnits()){
            throw new IllegalArgumentException("Both armies must have units");
        }
        if (armyOne == armyTwo){
            throw new IllegalArgumentException("Army cannot battle itself");
        }
        if (terrain == null){
            throw new IllegalArgumentException("Battle must have terrain");
        }
        this.armyOne = armyOne;
        this.armyTwo = armyTwo;
        this.terrain = terrain;
    }


    /**
     * @return
     */
    public Army simulate() {
        Random random = new Random();
        Army victor;
        while (armyOne.hasUnits() && armyTwo.hasUnits()) {
            // 0,
            int turn = random.nextInt(2);
            Unit armyOneUnit = armyOne.getRandom();
            Unit armyTwoUnit = armyTwo.getRandom();

            if (turn == 0) {
                armyOneUnit.attack(armyTwoUnit, this.terrain);
                if (armyTwoUnit.getHealth() <= 0) {
                    armyTwo.remove(armyTwoUnit);
                }
            } else {
                armyTwoUnit.attack(armyOneUnit, this.terrain);
                if (armyOneUnit.getHealth() <= 0) {
                    armyOne.remove(armyOneUnit);
                }
            }

        }
        if (armyOne.hasUnits()) victor = armyOne;
        else victor = armyTwo;
        return victor;
    }

    /**
     * @return
     */
    @Override
    public String toString() {
        return "Battle{" +
                "armyOne=" + armyOne +
                ", armyTwo=" + armyTwo +
                '}';
    }
}
