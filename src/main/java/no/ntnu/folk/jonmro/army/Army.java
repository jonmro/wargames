package no.ntnu.folk.jonmro.army;

import java.io.*;
import java.util.*;

import no.ntnu.folk.jonmro.units.*;

import java.util.stream.Collectors;

/**
 *
 */
public class Army {

    Random rand = new Random();
    private String name;
    private List<Unit> units;

    /**
     * @param name
     */
    public Army(String name) {
        this.name = name;
        this.units = new ArrayList<Unit>();
    }

    /**
     * @param name
     * @param units
     */
    public Army(String name, List<Unit> units) {
        this.name = name;
        this.units = units;
    }

    /**
     * @return
     */
    public String getName() {
        return this.name;
    }

    /**
     * @param unit
     */
    public void add(Unit unit){
        this.units.add(unit);
    }

    /**
     * @param units
     */
    public void addAll(List<Unit> units){
        this.units.addAll(units);
    }

    /**
     * @param unit
     */
    public void remove(Unit unit){
        this.units.remove(unit);
    }

    /**
     * @return
     */
    public boolean hasUnits() {
        return !this.units.isEmpty();
    }

    /**
     * @return
     */
    public List<Unit> getAllUnits() {
        return this.units;
    }

    /**
     * @return
     */
    public Unit getRandom() {
        // 0,
        return units.get(rand.nextInt(units.size()));
    }

    /**
     * @return
     */
    @Override
    public String toString() {
        return "Army{" +
                "name='" + name + '\'' +
                ", units=" + units +
                '}';
    }

    /**
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Army army = (Army) o;
        return Objects.equals(name, army.name) && Objects.equals(units, army.units);
    }

    /**
     * @return
     */
    @Override
    public int hashCode() {
        return Objects.hash(name, units);
    }


    public List<Unit> getInfantryUnits() {
        return units.stream().filter(u -> u instanceof InfantryUnit).collect(Collectors.toList());
    }

    public List<Unit> getCavelryUnits() {
        return units.stream().filter(u -> u instanceof CavalryUnit).collect(Collectors.toList());
    }

    public List<Unit> getRangedUnits() {
        return units.stream().filter(u -> u instanceof RangedUnit).collect(Collectors.toList());
    }

    public List<Unit> getCommanderUnits() {
        return units.stream().filter(u -> u instanceof CommanderUnit).collect(Collectors.toList());
    }

    public void saveArmy(String path) {

        // Create file
        try{
            File file = new File(path);
            if (file.createNewFile()) {
                System.out.println("Created army file"+ file.getName());
            } else {
                System.out.println("Failed to create file, file already exists");
            }
        } catch (IOException e) {
            System.out.println("Error occurred when trying to create file");
            e.printStackTrace();
        }

        // Write army to file
        FileWriter writer = null;
        try{
            writer = new FileWriter(path);
            writer.write(this.name+"\n");
            for (Unit unit: this.units) {
                writer.write(unit.toStringFile());
            }

        } catch (IOException e) {
            System.out.println("Error occurred when trying to write to file");
            e.printStackTrace();
        }finally {
            try {
                assert writer != null;
                writer.close();
            } catch (IOException e) {
                System.out.println("Error occurred when trying to close to file");
                e.printStackTrace();
            }
        }

    }

    public void loadArmy(String path) {
        String line = "";
        String delimiter = ",";
        // Creates unit objects and adds them to army object
        try {
            BufferedReader bufferedFile = new BufferedReader(new FileReader(path));

            while ((line = bufferedFile.readLine()) != null) {
                String[] unitData = line.split(delimiter);
                if (Objects.equals(unitData[0], "no.ntnu.folk.jonmro.units.CavalryUnit")) {
                    this.units.add(new CavalryUnit(unitData[1], Integer.parseInt(unitData[2])));
                } else if (Objects.equals(unitData[0], "no.ntnu.folk.jonmro.units.CommanderUnit")) {
                    this.units.add(new CommanderUnit(unitData[1], Integer.parseInt(unitData[2])));
                }else if (Objects.equals(unitData[0], "no.ntnu.folk.jonmro.units.InfantryUnit")) {
                    this.units.add(new InfantryUnit(unitData[1],Integer.parseInt(unitData[2])));
                }else if (Objects.equals(unitData[0], "no.ntnu.folk.jonmro.units.RangedUnit")) {
                    this.units.add(new RangedUnit(unitData[1],Integer.parseInt(unitData[2])));
                } else {
                    this.name = unitData[0];
                }
            }
        } catch (IOException e) {
            System.out.println("Error occurred when trying to open to file");
            e.printStackTrace();
        }
    }

/* potential use for selecting armies from GUI
    public String getArmyFileNames(String path) {

        return null;
    }
*/

}
