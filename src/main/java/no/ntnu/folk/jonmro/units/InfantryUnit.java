package no.ntnu.folk.jonmro.units;

/**
 *
 */
public class InfantryUnit extends Unit{
    /**
     * @param name
     * @param health
     * @param attack
     * @param armor
     */
    public InfantryUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
    }

    /**
     * @param name
     * @param health
     */
    public InfantryUnit(String name, int health) {
        super(name, health, 15, 10);
    }

    /**
     * @return
     */
    @Override
    public int getAttackBonus(String terrain) {

        switch (terrain) {
            case "FOREST":
                return 2+2;
            case "HILL":
                return 2;
            case "PLAINS":
                return 2;
            default:
                throw new IllegalArgumentException("Invalid terrain");
        }

    }

    /**
     * @return
     */
    @Override
    public int getResistBonus(String terrain) {
        switch (terrain) {
            case "FOREST":
                return 1+2;
            case "HILL":
                return 1;
            case "PLAINS":
                return 1;
            default:
                throw new IllegalArgumentException("Invalid terrain");
        }

    }


}
