package no.ntnu.folk.jonmro.units;

/**
 *
 */
public abstract class Unit {

    private String name;
    private int health;
    private int attack;
    private int armor;


    /**
     * @param name
     * @param health
     * @param attack
     * @param armor
     */
    public Unit(String name, int health, int attack, int armor) {
        this.name = name;
        this.health = health;
        this.attack = attack;
        this.armor = armor;
    }


    /**
     * @param opponent
     */
    public void attack(Unit opponent, String terrain) {
       opponent.setHealth(opponent.getHealth()-(this.attack+this.getAttackBonus(terrain))+(opponent.getArmor()+opponent.getResistBonus(terrain)));
    }


    /**
     * @return
     */
    public String getName() {
        return this.name;
    }

    /**
     * @return
     */
    public int getHealth() {
        return this.health;
    }

    /**
     * @return
     */
    public int getAttack() {
        return this.attack;
    }

    /**
     * @return
     */
    public int getArmor() {
        return this.armor;
    }

    /**
     * @param health
     */
    public void setHealth(int health) {
        this.health = health;
    }

    /**
     * @return
     */
    @Override
    public String toString() {
        return "units.Unit{" +
                "name='" + name + '\'' +
                ", health=" + health +
                ", attack=" + attack +
                ", armor=" + armor +
                '}';
    }

    public String toStringFile() {
        return this.getClass().getName() +","+ name +","+ health + "\n";
    }

    /**
     * @return
     */
    public abstract int getAttackBonus(String terrain);

    /**
     * @return
     */
    public abstract int getResistBonus(String terrain);


}
