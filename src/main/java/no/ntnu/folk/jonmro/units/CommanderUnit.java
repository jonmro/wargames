package no.ntnu.folk.jonmro.units;

/**
 *
 */
public class CommanderUnit extends CavalryUnit{

    private int turn = 0;

    /**
     * @param name
     * @param health
     * @param attack
     * @param armor
     */
    public CommanderUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
    }

    /**
     * @param name
     * @param health
     */
    public CommanderUnit(String name, int health) {
        super(name, health, 25, 15);
    }

    /**
     * @return
     */
    @Override
    public int getAttackBonus(String terrain) {
        if (turn == 0) {
            turn++;
            return 6;
        } else {
            return 2;
        }
    }


    /**
     * @return
     */
    @Override
    public int getResistBonus(String terrain) {
        return 1;
    }
}
