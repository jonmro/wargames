package no.ntnu.folk.jonmro.units;

/**
 *
 */
public class RangedUnit extends Unit{

    private int turn = 0;

    /**
     * @param name
     * @param health
     * @param attack
     * @param armor
     */
    public RangedUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
    }

    /**
     * @param name
     * @param health
     */
    public RangedUnit(String name, int health) {
        super(name, health, 15, 8);
    }


    /**
     * @return
     */
    @Override
    public int getAttackBonus(String terrain) {
        switch (terrain) {
            case "FOREST":
                return 2;
            case "HILL":
                return 3+2;
            case "PLAINS":
                return 3;
            default:
                throw new IllegalArgumentException("Invalid terrain");
        }
    }


    /**
     * @return
     */
    @Override
    public int getResistBonus(String terrain) {
        if (turn == 0) {
            turn++;
            return 6;
        } else if (turn == 1){
            turn++;
            return 4;
        } else {
            turn++;
            return 2;
        }
    }
}
