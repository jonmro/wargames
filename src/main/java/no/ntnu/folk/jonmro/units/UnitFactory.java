package no.ntnu.folk.jonmro.units;
import no.ntnu.folk.jonmro.units.*;

import java.util.ArrayList;
import java.util.List;

public class UnitFactory {

    public static Unit createUnit(String type, String name, int health) throws IllegalArgumentException {

        if (type.equals("CommanderUnit")) {
            return new CommanderUnit(name, health);
        } else if (type.equals("CavalryUnit")) {
            return new CavalryUnit(name, health);
        } else if (type.equals("RangedUnit")) {
            return new RangedUnit(name, health);
        }else if (type.equals("InfantryUnit")) {
            return new InfantryUnit(name, health);
        }else{
            throw new IllegalArgumentException(type + "is not a unit");
        }
    }

    public static ArrayList<Unit> createUnits(String type, String name, int health, int n) throws IllegalArgumentException {
        ArrayList<Unit> units = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            units.add(createUnit(type, name, health));
        }
        return units;
    }




}
