package no.ntnu.folk.jonmro.units;

/**
 *
 */
public class CavalryUnit extends Unit{

    private int turn = 0;

    /**
     * @param name
     * @param health
     * @param attack
     * @param armor
     */
    public CavalryUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
    }

    /**
     * @param name
     * @param health
     */
    public CavalryUnit(String name, int health) {
        super(name, health, 20, 12);
    }

    /**
     * @return
     */
    @Override
    public int getAttackBonus(String terrain) {
        switch (terrain) {
            case "FOREST":
                if (turn == 0) {
                    turn++;
                    return 6;
                } else {
                    return 2;
                }
            case "HILL":
                if (turn == 0) {
                    turn++;
                    return 6;
                } else {
                    return 2;
                }
            case "PLAINS":
                if (turn == 0) {
                    turn++;
                    return 6+2;
                } else {
                    return 2+2;
                }
            default:
                throw new IllegalArgumentException("Invalid terrain");
        }


    }


    /**
     * @return
     */
    @Override
    public int getResistBonus(String terrain) {
        switch (terrain) {
            case "FOREST":
                return 0;
            case "HILL":
                return 1;
            case "PLAINS":
                return 1;
            default:
                throw new IllegalArgumentException("Invalid terrain");
        }
    }
}
