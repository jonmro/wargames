module no.ntnu.folk.jonmro {
    requires javafx.controls;
    requires javafx.fxml;

    opens no.ntnu.folk.jonmro to javafx.fxml;
    exports no.ntnu.folk.jonmro;
    opens no.ntnu.folk.jonmro.battle to javafx.fxml;

}
